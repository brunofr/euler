;; Project Euler: problem 1
;;
;; If we list all the natural numbers below 10 that are multiples of 3
;; or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
;;
;; Find the sum of all the multiples of 3 or 5 below 1000.
;;
(ns euler.pr1)

(defn eu1-pencil
  "pencil solution to problem euler #1"
  [m]
  (letfn [(sum [n] (/ (+ (* n n) n) 2))
          (summ [n m] (* n (sum (quot (- m 1) n))))]
    (- (+ (summ 3 m) (summ 5 m)) (summ 15 m)))
  )

(defn eu1
  [n]  
  (reduce + (filter #(or (zero? (rem %1 3)) (zero? (rem %1 5))) (range 1 n))))
