;; Project Euler: problem 3
;;
;; The prime factors of 13195 are 5, 7, 13 and 29.
;;
;; What is the largest prime factor of the number 600851475143 ?
;;
(ns euler.pr3)

(defn factor
  "finds the largest prime factor of a number"
  [n div]
  (if (= n div)
    n
    (if (zero? (rem n div))
      (recur (quot n div) div)
      (recur n (inc div)))))
