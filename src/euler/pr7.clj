;; Project Euler: problem 7
;;
;; By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we
;; can see that the 6th prime is 13.
;;
;; What is the 10001st prime number?
;;
(ns euler.pr7)

(defn divisible?
  "returns true if num is divisible by any of the elements in the coll"
  [num coll]
  (let [div (first coll)]
    (if (zero? div)
      false
      (if (zero? (rem num div))
        true
        (if (empty? (rest coll))
          false
          (recur num (rest coll)))))))

;; could be optimised to walk only even numbers, and only up to square
;; of num
(defn next-prime
  "given a seq, cons the next larger prime"
  ([coll] (next-prime coll (inc (first coll))))
  ([coll num]
     (if (divisible? num coll)
       (recur coll (inc num))
       (cons num coll))))

(defn primes
  "lazy sequence of prime numbers"
  []
  (map first (iterate next-prime [2])))
