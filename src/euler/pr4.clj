;; Project Euler: problem 4
;;
;; A palindromic number reads the same both ways. The largest
;; palindrome made from the product of two 2-digit numbers is 9009 =
;; 91  99.
;;
;; Find the largest palindrome made from the product of two 3-digit numbers.
;;
(ns euler.pr4
  (:use clojure.contrib.generic.math-functions))

(defn palindromic?
  "tests if a number is palindromic"
  [x]
  (= (reverse (str x)) (seq (str x))))

(defn palindromics
  "lazy sequence of decreasing palindromic numbers"
  []
  (filter palindromic? (iterate #(- % 1) 999999)))

(defn divisor3
  "returns the highest, if any, 3-digit divisor"
  [x]
  (some #(if (zero? (rem x %)) [x % (/ x %)]) (range 999 (sqrt x) -1)))

(last (take-while #(= nil (divisor3 %)) (palindromics)))
