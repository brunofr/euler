;; Project Euler: problem 6
;;
;; The sum of the squares of the first ten natural numbers is,
;;
;; 1^2 + 2^2 + ... + 10^2 = 385
;;
;; The square of the sum of the first ten natural numbers is,
;;
;; (1 + 2 + ... + 10)^2 = 55^2 = 3025
;;
;; Hence the difference between the sum of the squares of the first ten
;; natural numbers and the square of the sum is 3025 - 385 = 2640.
;;
;; Find the difference between the sum of the squares of the first one
;; hundred natural numbers and the square of the sum.
;;
(ns euler.pr6)

(defn sol
  "solution to problem"
  [num]
  (letfn [(square [num] (* num num))
          (squares [] (map #(* % %) (iterate inc 1)))]
    (- (square (reduce + (take num (iterate inc 1))))
       (reduce + (take num (squares))))))
