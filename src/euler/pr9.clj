;; Project Euler: problem 9
;;
;; A Pythagorean triplet is a set of three natural numbers, a  b  c,
;; for which,
;;
;; a^2 + b^2 = c^2
;;
;; For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.
;;
;; There exists exactly one Pythagorean triplet for which,
;;
;; a + b + c = 1000
;;
;; Find the product abc.
;;
(ns euler.pr9)

;; we can use a brute force, and very slow, solution by simply
;; iterating over all positive integers to materialize the sequence of
;; pythagorean triplets. the berggren-transform is much more
;; effective as it generates all triples from the initial [3 4 5]
;; triangle.

(defn pythagorean-triplet?
  "tests a triplet to see if it's a pythagorian one, where a^2 + b^2 = c^2"
  [[a b c]]
  (and (= (* c c) (+ (* a a) (* b b))) (< a b)))

(defn pythagorean-triplets
  "generates a sequence of pythagorian triplets"
  [n]
  (for [a (range 1 (/ n 2)) b (range 1 (/ n 2)) c (range 1 (/ n 2))
        :when (and (pythagorean-triplet? [a b c]) (> (inc n) (+ a b c)))]
    [a b c]))

(defn check-sol
  [n]
  (filter #(= n (first %)) (map (fn [[a b c]] [(+ a b c) (* a b c) a b c])
                                (pythagorean-triplets n))))


(defn berggren-transform
  "all primitive Pythagorean triples can be generated from the (3, 4,
  5) triangle by using the 3 linear transformations T1, T2, T3"
  [[a b c]]
  (letfn [(t1a [[a b c]] (+ (- a (* 2 b)) (* 2 c)))
          (t2a [[a b c]] (+ a (* 2 b) (* 2 c)))
          (t3a [[a b c]] (- (+ (* 2 b) (* 2 c)) a))
          (t1b [[a b c]] (+ (- (* 2 a) b) (* 2 c)))
          (t2b [[a b c]] (+ (+ (* 2 a) b) (* 2 c)))
          (t3b [[a b c]] (+ (- b (* 2 a)) (* 2 c)))
          (t1c [[a b c]] (+ (- (* 2 a) (* 2 b)) (* 3 c)))
          (t2c [[a b c]] (+ (+ (* 2 a) (* 2 b)) (* 3 c)))
          (t3c [[a b c]] (+ (- (* 2 b) (* 2 a)) (* 3 c)))]
    [
     [(t1a [a b c]) (t1b [a b c]) (t1c [a b c])]
     [(t2a [a b c]) (t2b [a b c]) (t2c [a b c])]
     [(t3a [a b c]) (t3b [a b c]) (t3c [a b c])]]))
