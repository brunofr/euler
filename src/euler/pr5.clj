;; Project Euler: problem 5
;;
;; 2520 is the smallest number that can be divided by each of the
;; numbers from 1 to 10 without any remainder.
;;
;; What is the smallest positive number that is evenly divisible by
;; all of the numbers from 1 to 20?
;;
(ns euler.pr5)

(defn divisible?
  "returns true if num is divisible for all integers 1 to div"
  [num div]
  (if (= 1 div)
    true
    (if (zero? (rem num div))
      (recur num (dec div))
      false)))

(defn sol5
  "solution to problem 5"
  ([div] (sol5 1 div))
  ([num div] (if (divisible? num div)
               num
               (recur (inc num) div)))
  )

