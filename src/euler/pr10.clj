;; Project Euler: problem 10
;;
;; The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
;;
;; Find the sum of all the primes below two million.
(ns euler.pr10
  (:use [clojure.contrib.lazy-seqs :only (primes)]))

(defn sol10
  []
  (reduce + (take-while #(> 2000000 %) primes)))


