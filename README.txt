# Euler

These are my solutions to [Project Euler](http://projecteuler.net)'s
problems, using Clojure. Whenever obvious I made some "pencil"
optimisations, otherwise the solutions favor the computational
solution.

## Usage

Each problem solution is in a different file, e.g. pr7.clj is the
solution to problem 7.

## License

Copyright (C) 2011 Bruno Fernandez-Ruiz

Distributed under the MIT license.
